#!/usr/bin/env python
# encoding: utf-8

import csv
import os
import argparse
from pathlib import Path
from string import Template

PARSER = argparse.ArgumentParser("jacoco", description="affiche la couverture jacoco")
JCC_FILE = "target/site/jacoco/jacoco.csv"
XML_FILE = "coverage.xml"

def main():
    args = PARSER.parse_args()
    with open(JCC_FILE) as myfile:
        csv_reader = csv.DictReader(myfile)
        covered_nb = 0
        missed_nb = 0
        for row in csv_reader:
            covered_nb += int(row["LINE_COVERED"])
            missed_nb += int(row["LINE_MISSED"])
    coverage = 100 * float(covered_nb) / (covered_nb + missed_nb)
    print("Couverture Jacoco depuis %s" %  JCC_FILE)
    print("(%s%%)" % int(coverage))
    template = Path("templates/coverage.xml").read_text()
    rendered_template = Template(template).substitute(
            LINES_VALID=covered_nb+missed_nb,
            LINES_COVERED=covered_nb,
            LINE_RATE=coverage
            )
    print(f"Coverage has been generated to {XML_FILE}: {coverage=}")
    Path(XML_FILE).write_text(rendered_template)


if __name__ == "__main__":
    main()

